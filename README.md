# Fantasy Football Tooling

This is a suite of tools for fantasy football.

## Getting Started

```bash
# Install dependencies
yarn

# Copy and setup the env file
# with env variables
cp .env.template .env

# Get players.json from
# Fantasy Footballers UDK
yarn udk

# Copy the output from UDK
# to the input for Sleeper
yarn copy

# Display the available players
# from players.json in a Sleeper draft
yarn sleeper
```

## JSON file format

If you don't have the Fantasy Footballer's Ultimate Draft Kit, or you prefer to form your own `players.json` input for `yarn sleeper`, you may do so. Here is the format for the JSON file:

```js
{
  "qb": [
    {
      "name": "Tier X",
      "players": [
        {
          // required properties
          "name": "Tom Brady",
          "rank": 19,
          // optional properties
          "team": "NE",
          "points": "269.3",
          "risk": "2.3",
          "adp": "11.05"
        }
      ]
    }
  ],
  "rb": [...],
  "wr": [...],
  "te": [...],
  "k": [...],
  "def": [...]
}
```

If you don't wish to have the players by tier, make a single tier with `"name": "All"`. This is treated in a special way.
