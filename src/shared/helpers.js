const puppeteer = require('puppeteer');

const envVarToBoolean = envVar => envVar && envVar.toLowerCase() === 'true';

const getIsInDebugMode = () => envVarToBoolean(process.env.DEBUG);

const getPuppeteerOptions = () => {
  const options = {
    defaultViewport: {
      width: 1400,
      height: 900,
    },
  };

  if (getIsInDebugMode()) {
    return {
      ...options,
      headless: false,
    };
  }

  return options;
}

const handlePageConsole = (page) => {
  if (getIsInDebugMode()) {
    page.on('console', msg => console.log('PAGE LOG:', msg.text()));
  }
}

const openBrowser = async () => {
  return await puppeteer.launch(getPuppeteerOptions());
}

const closeBrowser = async (browser) => {
  return await browser.close();
}

const openPage = async (browser) => {
  const page = await browser.newPage();
  handlePageConsole(page);
  return page;
}

module.exports = {
  envVarToBoolean,
  getIsInDebugMode,
  getPuppeteerOptions,
  handlePageConsole,
  openBrowser,
  closeBrowser,
  openPage,
};
