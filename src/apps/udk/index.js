require('dotenv').config();
const path = require('path');
const fs = require('fs-extra');

const {
  openBrowser,
  closeBrowser,
  openPage,
} = require('../../shared/helpers');
const { validatePlayers } = require('./helpers');

const udkUrl = 'https://www.thefantasyfootballers.com/2019-ultimate-draft-kit';
const udkRankingsUrl = `${udkUrl}/udk-position-rankings`;
const udkRankingsUrlsByPosition = {
  qb: `${udkRankingsUrl}?position=QB`,
  rb: `${udkRankingsUrl}?position=RB`,
  wr: `${udkRankingsUrl}?position=WR`,
  te: `${udkRankingsUrl}?position=TE`,
  k: `${udkUrl}/udk-kickers-rankings`,
  def: `${udkUrl}/udk-defense-rankings`,
};

const scrape = async () => {
  const browser = await openBrowser();
  const page = await openPage(browser);

  const ensureLoggedIn = async (page) => {
    const getIsLoggedIn = async () => {
      const loggedInPromise = page.waitForSelector('.UDKRankingsTable tbody tr').then(() => true);
      const notLoggedInPromise = page.waitForSelector('#user_login').then(() => false);
      return await Promise.race([loggedInPromise, notLoggedInPromise]);
    }

    if (!(await getIsLoggedIn())) {
      await page.type('#user_login', process.env.UDK_USERNAME);
      await page.type('#user_pass', process.env.UDK_PASSWORD);
      await page.click('#wp-submit');

      if (!(await getIsLoggedIn())) {
        throw new Error('Login failed');
      }
    }
  };

  const getPlayersByTier = async (page, position, hasTiers = true) => {
    const url = udkRankingsUrlsByPosition[position];
    await page.goto(url);
    await ensureLoggedIn(page);
    await page.waitForSelector('.UDKRankingsTable tbody tr');
    return await page.evaluate(({
      hasTiers,
      position,
    }) => {
      const cells = document.querySelectorAll('.UDKRankingsTable tbody tr');
      const tiers = [];
      let currRank = 1;
      cells.forEach(cell => {
        const classNames = cell.className.split(' ');
        const isNewTier = classNames.includes('group');
        if (isNewTier) {
          const tierName = cell.firstChild.innerText;
          tiers.push({
            name: tierName,
            players: [],
          });
        } else {
          let playerTeam;
          let points;
          let risk;
          let adp;
          let andyRank;
          let jasonRank;
          let mikeRank;
          // if we don't have tiers, we need to make one for "All"
          if (!hasTiers && !tiers.length) {
            tiers.push({
              name: 'All',
              players: [],
            });
          }
          // get values present for all positions
          const playerName = cell.firstChild.firstChild.lastChild.children[0].innerText;
          // get values only present for some positions
          if (position === 'def' || position === 'k') {
            andyRank = cell.children[2].innerText;
            jasonRank = cell.children[3].innerText;
            mikeRank = cell.children[4].innerText;
          } else {
            points = cell.children[2].innerText;
            risk = cell.children[3].innerText;
            adp = cell.children[4].innerText;
          }
          if (position !== 'def') {
            playerTeam = cell.firstChild.firstChild.lastChild.children[1].innerText // 'HOU (10)'
              .split('(')[0] // 'HOU '
              .trim(); // 'HOU'
          }
          // add player to the current tier
          const currTier = tiers[tiers.length - 1];
          currTier.players.push({
            name: playerName,
            team: playerTeam,
            rank: currRank,
            points,
            risk,
            adp,
            andyRank,
            jasonRank,
            mikeRank,
          });
          currRank += 1;
        }
      });
      return tiers;
    }, {
      hasTiers,
      position,
    });
  };

  const players = {
    qb: await getPlayersByTier(page, 'qb'),
    rb: await getPlayersByTier(page, 'rb'),
    wr: await getPlayersByTier(page, 'wr'),
    te: await getPlayersByTier(page, 'te'),
    k: await getPlayersByTier(page, 'k', false),
    def: await getPlayersByTier(page, 'def', false),
  }

  await closeBrowser(browser);

  validatePlayers(players);

  const outputFilename = path.join(__dirname, 'output', 'players.json');
  fs.outputJsonSync(outputFilename, players, { spaces: 2 });
};

scrape();
