const chalk = require('chalk');

const validatePlayers = (players) => {
  Object.entries(players).forEach(([pos, tiers]) => {
    tiers.forEach((tier) => {
      tier.players.forEach((player) => {
        if(
          !player.name.includes(' ')
          || player.name.includes('(')
          || player.name.includes(')')
        ) {
          console.log(chalk.yellow(`Warning: Player name "${player.name}" may be incorrect.`));
        }
      });
    });
  });
}

module.exports = {
  validatePlayers,
};
