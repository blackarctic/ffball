const getPlayersForPosition = async (page, position) => {
  await page.waitForSelector('.draft-board');
  return await page.evaluate((position) => {
    // get cells containing info
    const firstNameCells = document.querySelectorAll(`.draft-board .cell.${position} .first-name`);
    const lastNameCells = document.querySelectorAll(`.draft-board .cell.${position} .last-name`);
    const teamCells = document.querySelectorAll(`.draft-board .cell.${position} .position`);
    // declare buffer arrays for info
    const firstNames = [];
    const lastNames = [];
    const teams = [];
    // build up buffer arrays
    firstNameCells.forEach(cell => firstNames.push(cell.innerText));
    lastNameCells.forEach(cell => lastNames.push(cell.innerText));
    teamCells.forEach(cell => teams.push(cell.innerText.split('-')[1].trim()));
    // declare players array
    const players = [];
    // build up players array with info from buffer arrays
    firstNameCells.forEach((_, i) => {
      const firstName = firstNames[i];
      const lastName = lastNames[i];
      const team = teams[i];

      players[i] = {
        name: `${firstName} ${lastName}`,
        team,
      }
    });
    // return players array
    return players;
  }, position);
}

module.exports = {
  getPlayersForPosition,
}
