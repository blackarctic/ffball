require('dotenv').config();

const path = require('path');
const fs = require('fs-extra');
const _ = require('lodash');
const chalk = require('chalk');

const {
  openBrowser,
  closeBrowser,
  openPage,
} = require('../../shared/helpers');

const {
  getPlayersForPosition,
} = require('./browserScripts');

const visibleTiersCount = Number(process.env.SLEEPER_VISIBLE_TIERS_COUNT);
const visibleNonTiersPlayerCount = Number(process.env.SLEEPER_VISIBLE_NON_TIER_PLAYERS_COUNT);

const inputFilename = path.join(__dirname, 'input', 'players.json');
const rankedPlayers = fs.readJsonSync(inputFilename);

const scrape = async () => {
  const browser = await openBrowser();
  const page = await openPage(browser);
  await page.goto(process.env.SLEEPER_DRAFT_URL);

  const players = {
    def: await getPlayersForPosition(page, 'def'),
    k: await getPlayersForPosition(page, 'k'),
    qb: await getPlayersForPosition(page, 'qb'),
    rb: await getPlayersForPosition(page, 'rb'),
    te: await getPlayersForPosition(page, 'te'),
    wr: await getPlayersForPosition(page, 'wr'),
  }

  await closeBrowser(browser);

  return players;
};

const getAvailablePlayers = (draftedPlayers) => {
  const availablePlayers = _.cloneDeep(rankedPlayers);

  Object.entries(availablePlayers).forEach(([pos, tiers]) => {
    availablePlayers[pos] = tiers.filter((tier) => {
      tier.players = tier.players.filter((player) => {
        const isPlayerDrafted = draftedPlayers[pos].some((draftedPlayer) => {
          return draftedPlayer.name === player.name;
        });
        return !isPlayerDrafted;
      });
      return !!tier.players.length;
    });
  });

  return availablePlayers;
};

// TODO: better display format (thinking a table)
const displayAvailablePlayers = (availablePlayers) => {
  Object.entries(availablePlayers).forEach(([pos, tiers]) => {
    console.log(`*** ${pos.toUpperCase()} ***`);
    tiers.forEach((tier, tierIndex) => {
      console.log(`  === ${tier.name} ===`);
      let hasDisplayedEllipsis = false;
      tier.players.forEach((player, playerIndex) => {
        let shouldDisplay = true;
        let color = 'white';

        if (tier.name === 'All') {
          if (playerIndex >= visibleNonTiersPlayerCount) {
            shouldDisplay = false;
          } else if (playerIndex === 0 || playerIndex === 1) {
            color = 'green';
          } else if (playerIndex === 2 || playerIndex === 3) {
            color = 'yellow';
          } else {
            color = 'gray';
          }
        } else {
          if (tierIndex >= visibleTiersCount) {
            shouldDisplay = false;
          } else if (tierIndex === 0) {
            color = 'green';
          } else if (tierIndex === 1) {
            color = 'yellow';
          } else {
            color = 'gray';
          }
        }
        if (shouldDisplay) {
          const formattedRank = `${player.rank}) `;
          const formattedTeam = player.team ? ` (${player.team})` : '';

          const formattedPoints = player.points ? ` | ${player.points} pts` : '';
          const formattedRisk = player.risk ? ` | ${player.risk} risk` : '';
          const formattedADP = player.adp ? ` | ${player.adp} ADP` : '';

          const formattedAndyRank = player.andyRank ? ` | ${player.andyRank}` : '';
          const formattedJasonRank = player.jasonRank ? ` | ${player.jasonRank}` : '';
          const formattedMikeRank = player.mikeRank ? ` | ${player.mikeRank}` : '';

          const formattedStandardInfo = chalk[color](
            `${formattedRank}${player.name}${formattedTeam}`
          );
          const formattedStats = chalk.gray(
            `${formattedPoints}${formattedRisk}${formattedADP}`
          );
          const formattedRanks = chalk.gray(
            `${formattedAndyRank}${formattedJasonRank}${formattedMikeRank}`
          );

          console.log(
            `    ${formattedStandardInfo}${formattedStats}${formattedRanks}`
          );

        } else if (!hasDisplayedEllipsis) {
          console.log(chalk.gray('    ...'));
          hasDisplayedEllipsis = true;
        }
      });
    });
    console.log();
    console.log();
  });
};

(async () => {
  const draftedPlayers = await scrape();
  const availablePlayers = getAvailablePlayers(draftedPlayers);
  displayAvailablePlayers(availablePlayers);
})();
